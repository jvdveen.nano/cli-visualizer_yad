# cli-visualizer_yad  

_(drag readme.html into firefox for soft reading)_
 
## A simple VIS (similar to audio visualizer CAVA) tweaker.   

##### __Works with Tuxedo KDE and Mabox (Manjaro Openbox).__

##### The application utilizes YAD to set basic parameters for VIS (audio visualizer). 


![menu](images/menu.png)

![welcome](images/welcome-color.png)	![pick](images/yad-colorizer.png)

![save](images/save-theme.png)	![confirm](images/save-theme-confirm.png)

---

### Launch `vis-manager` to change the `vis` variable. 

> __hotkey toggle:__ s = mono or stereo mode

> __hotkey refresh:__ r = reload config  _(after save settings, press 'r' to reload running vis)_

---

![jgmenu](images/jgmenu.png)

![jgmenu](images/kdemenu.png)



# Use `Install.sh` for the easy way (system wide). Tuxedo or Mabox. _(Uninstall script available)_

_(Install script looks for Ubuntu[ppa] or Arch)_

_(Uninstall only bin and .desktop files.  Configs need to be removed manualy)_

[README LONG](https://gitlab.com/muzlabz/cli-visualizer_yad/-/blob/main/README_LONG.md?ref_type=heads)

# Installation [manual ARCH]. 


```
yay -S cli-visualizer

sudo pacman -S terminator

sudo pacman -S yad

sudo pacman -S wmctrl
```

### Create the necessary directories and place the configuration files in the following locations:

`/usr/local/bin/transparent-vis.sh`  Terminator running custom vis, transparent. Concept from @napcok mabox dev.

_Note: play with font size for the best performance._
    
`/usr/local/bin/vis-manager.sh` Generate random colors from amount + vis config settings.

`/usr/local/bin/yad-select-file.sh` Color theme selection.

`/usr/local/bin/vis-yad-colors.sh` color picker for theming

`/usr/local/bin/rename-vis-colors.sh` Color theme selection.

`/usr/local/bin/mb-setvar` _Note: tool to change values of config files._ 

(I put it in, only for `NON Mabox` systems) creator @napcok. 

_A Mabox user can remove `mb-setvar` from the `/usr/local/bin` dir. The offical is placed in `/usr/bin`._
   
`~/.local/share/applications/transparent-vis.desktop`  Menu item. Terminator.
    
`~/.local/share/applications/vis-manager.desktop`   Menu item. Yad.

`~/.config/vis/mb-vis-config`  Custom vis config file.

`~/.config/vis/.amount`  Config file for amount colors.
    
`~/.config/vis/colors/mb-vis-colors`  Active color list.

`~/.config/vis/welcome-vis.txt`
    
`~/.config/terminator/vis-transparent` Config file for terminator.

### Ensure scripts are executable:

```
chmod +x bin/transparent-vis.sh

chmod +x bin/vis-manager.sh

chmod +x bin/yad-select-file.sh

chmod +x bin/mb-setvar

chmod +x bin/rename-vis-colors.sh

chmod +x bin/vis-yad-colors.sh

```

### Configure hotkey:

##### Add the following to ~/.config/openbox/rc.xml to bind a key to start transparent-vis.sh:

Two options for the size :  -full size or -half size width. *(default = full : height 200)*

    - transparent-vis.sh 500 half
    
```
<keybind key="C-A-v">
  <action name="Execute">
    <execute>transparent-vis.sh</execute>
  </action>
</keybind>
```

### Ensure Picom compositor excludes shadow for transparency:

```
shadow-exclude = [
    "name *= 'vistransparent'"
]
```

> #### Trouble shoot: 
> ##### `Issue:`  Value's are not changing when i `save the settings`. 
> ##### `Awnser:` Look at `mb-vis-config`. Remove `#` from the needed line(s), to be able to change the value.

---

![theme](images/theme.png) ![vis3](images/vis-3.gif) ![vis3](images/vis-4.gif) ![vis3](images/vis-5.gif) ![stereo1](images/stereo-one.gif)

#### Old version demo. New version first image at the top of the page. (Characters)

![demo](images/vis-demo.gif)

## Contributions:

_Contributions to improve and expand the functionality of these scripts are welcome. 
If you encounter any issues or have ideas for enhancements, feel free to submit a pull request or open an issue on GitLab._
