#!/bin/bash

# Function to install a package if not already installed (for Debian or Ubuntu)
FUNC_INSTALL_DEBIAN() {
    if dpkg -s "$1" &> /dev/null; then
        tput setaf 2
        echo "###############################################################################"
        echo "################## The package \"$1\" is already installed"
        echo "###############################################################################"
        echo
        tput sgr0
    else
        tput setaf 3
        echo "###############################################################################"
        echo "##################  Installing package \"$1\""
        echo "###############################################################################"
        echo
        sudo apt-get install -y "$1"
        tput sgr0
    fi
}

# Function to install a package if not already installed (for Arch Linux)
FUNC_INSTALL_ARCH() {
    if pacman -Qi "$1" &> /dev/null; then
        tput setaf 2
        echo "###############################################################################"
        echo "################## The package \"$1\" is already installed"
        echo "###############################################################################"
        echo
        tput sgr0
    else
        tput setaf 3
        echo "###############################################################################"
        echo "##################  Installing package \"$1\""
        echo "###############################################################################"
        echo
        yay -S --noconfirm --needed "$1"
    fi
}

CONFIG_INSTALL() {
    # Set the source and destination directories
    vis_source_dir=".config/vis"	
    terminator_source_dir=".config/terminator"
    local_applications_source_dir=".local/share/applications"
    bin_source_dir="bin"

    vis_config_dir="$HOME/.config/"
    terminator_config_dir_kde="$HOME/.config/kdedefaults/"
    terminator_config_dir="$HOME/.config/"
    local_applications_dir="$HOME/.local/share/"
    bin_dir="/usr/local/"
    	
    # Create destination directories if not exist
    mkdir -p "$vis_config_dir"
    mkdir -p "$terminator_config_dir_kde"
    mkdir -p "$terminator_config_dir"
    mkdir -p "$local_applications_dir"

	cp -rf "$vis_source_dir" "$vis_config_dir" 
	cp -rf "$terminator_source_dir" "$terminator_config_dir_kde"
	cp -rf "$terminator_source_dir" "$terminator_config_dir"
	cp -rf "$local_applications_source_dir" "$local_applications_dir"
	sudo cp -rf "$bin_source_dir" "$bin_dir"

    # Print completion message
    tput setaf 5
    echo "################################################################"
    echo "Scripts have been installed in $bin_dir and configs put in place ... "
    echo "################################################################"
    echo
    tput sgr0

    # Display commands and wait for user input
	if [[ -f /etc/lsb-release && "$(grep 'DISTRIB_ID=Ubuntu' /etc/lsb-release)" ]]; then
    tput setaf 6
    echo "UBUNTU, installing cli-visualizer from github..."
    echo "First installing dependencies with apt. Adding PPA ubuntu-toolchain-r/test for gcc"
    tput sgr0
    read -p "Enter to continue process. Ctrl-c to cancel."
	fi

	if [ -f /etc/debian_version ]; then
	tput setaf 6
    echo "DEBIAN, installing cli-visualizer from github..."
    echo "First installing dependencies with apt."
    tput sgr0
    read -p "Enter to continue process. Ctrl-c to cancel."
	fi
	
    tput setaf 1
    echo "################################################################"
    echo "Cli-visualizer_yad.sh Installed!!! Check menu for vis manager"
    echo "################################################################"
    echo
    echo "For more information about cli-visualizer, visit:"
    echo "https://github.com/dpayne/cli-visualizer?tab=readme-ov-file#debian--ubuntu"
    tput sgr0
}

check_update() {
    # Check if the system is up to date for Arch Linux
    if [ -f /etc/arch-release ]; then
        yay -Syu
    fi
}

show_warning() {
    # Set warning message based on the distribution
    if [ -f /etc/arch-release ]; then
        # Warning message for Arch Linux
        clear
        warning_message="This script will install packages on your system. Do you accept?"
    elif [[ -f /etc/lsb-release && "$(grep 'DISTRIB_ID=Ubuntu' /etc/lsb-release)" ]]; then
        # Warning message for Ubuntu
        clear
        warning_message="This script will install packages. Continue at your own risk."
    elif [ -f /etc/debian_version ]; then
		# Warning message for Debian
        clear
        warning_message="This script will install packages. Continue at your own risk."
    else
		clear
        echo "This script is not supported on this Linux distribution."
        exit 1
    fi

    # Show the warning message in the terminal
    echo "$warning_message"
	echo ""
    # Prompt user to confirm
    read -p "Do you want to continue? (y/n): " response

    # Check user response
    case $response in
        [yY])
            # User confirmed, continue with the script
            ;;
        *)
            # User declined, exit the script
            echo "Operation canceled by user."
            exit 1
            ;;
    esac
}

# Check if the script is running on supported distributions
show_warning

# Check if the file /etc/arch-release exists
if [ -f /etc/arch-release ]; then
    echo "This is an Arch-based system."
    check_update
    # List of packages to install for Arch Linux
    arch_packages=(
        cli-visualizer
        terminator
        wmctrl
        yad
        font-awesome-5 
    )

    # Install packages from the list for Arch Linux
    count=0
    for name in "${arch_packages[@]}"; do
        ((count++))
        tput setaf 3
        echo "Installing package nr. $count: $name"
        tput sgr0
        FUNC_INSTALL_ARCH "$name"
    done

    CONFIG_INSTALL
    
# Check if the file /etc/lsb-release exists (Ubuntu)
elif [[ -f /etc/lsb-release && "$(grep 'DISTRIB_ID=Ubuntu' /etc/lsb-release)" ]]; then
    tput setaf 6
    echo "This is an Ubuntu-based system."
    echo ""
    read -p "Enter to upgrade system and after that installing needed packages."
    tput sgr0
    sudo apt update ; sudo apt upgrade -y
    clear
    FUNC_INSTALL_DEBIAN libfftw3-dev
    FUNC_INSTALL_DEBIAN libncursesw5-dev
    FUNC_INSTALL_DEBIAN cmake
    FUNC_INSTALL_DEBIAN libpulse-dev
    FUNC_INSTALL_DEBIAN yad
    FUNC_INSTALL_DEBIAN wmctrl
    FUNC_INSTALL_DEBIAN terminator
    FUNC_INSTALL_DEBIAN fonts-font-awesome
    
    CONFIG_INSTALL

    # Adding a PPA for newer gcc compiler if it doesn't exist
    if ! grep -q "^deb .*ubuntu-toolchain-r/test" /etc/apt/sources.list /etc/apt/sources.list.d/*; then
        echo "Adding PPA for Ubuntu toolchain"
        FUNC_INSTALL_DEBIAN software-properties-common
        sudo add-apt-repository ppa:ubuntu-toolchain-r/test -y
        sudo apt-get update ; sudo apt upgrade -y
        FUNC_INSTALL_DEBIAN build-essential
        FUNC_INSTALL_DEBIAN g++
    else
        echo "PPA for Ubuntu toolchain already exists."
    echo "Install cli-visualizer for Ubuntu from GitHub"
    echo ""
    mkdir -p cli-visualizer
    cd cli-visualizer || exit
    git clone https://github.com/dpayne/cli-visualizer.git
    cd cli-visualizer || exit
    chmod +x install.sh
    ./install.sh
	fi
	
# Check if the file /etc/debian_version exists (Debian)
elif [ -f /etc/debian_version ]; then
    tput setaf 6
    echo "This is a Debian-based system."
    echo ""
    read -p "Enter to upgrade system and after that installing needed packages."
    tput sgr0
    sudo apt update ; sudo apt upgrade -y
    clear
    FUNC_INSTALL_DEBIAN libfftw3-dev
    FUNC_INSTALL_DEBIAN libncursesw5-dev
    FUNC_INSTALL_DEBIAN cmake
    FUNC_INSTALL_DEBIAN libpulse-dev
    FUNC_INSTALL_DEBIAN yad
    FUNC_INSTALL_DEBIAN wmctrl
    FUNC_INSTALL_DEBIAN terminator
    FUNC_INSTALL_DEBIAN build-essential
    FUNC_INSTALL_DEBIAN g++
    FUNC_INSTALL_DEBIAN fonts-font-awesome
    
    CONFIG_INSTALL

    echo "Install cli-visualizer for Debian from GitHub"
    git clone https://github.com/dpayne/cli-visualizer.git
    cd cli-visualizer || exit
    chmod +x install.sh
    ./install.sh
fi
