#!/bin/bash

# Function to remove specific files and directories ROOT
func_remove_files_root() {
    tput setaf 3
    echo "###############################################################################"
    echo "##################  Removing $1"
    echo "###############################################################################"
    echo
    tput sgr0
    sudo rm -rf "$2"
}


# Function to remove specific files and directories HOME
func_remove_files_home() {
    tput setaf 3
    echo "###############################################################################"
    echo "##################  Removing $1"
    echo "###############################################################################"
    echo
    tput sgr0
    sudo rm -rf "$2"
}


# Set the directories and files to be removed
bin_dir="/usr/local/bin"
local_applications_dir="$HOME/.local/share/applications"

# Remove specific bin files related to the software
func_remove_files_root "vis-manager.sh" "$bin_dir/vis-manager.sh"
func_remove_files_root "yad-select-file.sh" "$bin_dir/yad-select-file.sh"
func_remove_files_root "mb-setvar" "$bin_dir/mb-setvar"
func_remove_files_root "transparent-vis.sh" "$bin_dir/transparent-vis.sh"
func_remove_files_root "vis-color-generator.sh" "$bin_dir/vis-color-generator.sh"
func_remove_files_root "vis-yad-colors.sh" "$bin_dir/vis-color-generator.sh"

# Remove .desktop files specific to the software
func_remove_files_home "vis-manager.desktop" "$local_applications_dir/vis-manager.desktop"
func_remove_files_home "transparent-vis.desktop" "$local_applications_dir/transparent-vis.desktop"

tput setaf 11
echo "################################################################"
echo "Uninstallation of specific files completed"
echo "################################################################"
echo
tput sgr0
