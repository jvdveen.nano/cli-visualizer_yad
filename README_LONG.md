# VIS Configurator README

This repository contains a set of Bash scripts designed to configure and manage a customizable visual equalizer (VIS) for use with music players such as Amarok. The scripts offer a user-friendly interface for configuring various aspects of the VIS, including color theming and visualizer characters. Below is an overview of each script and its purpose:

## Compatibility:

The VIS Configurator scripts have been tested primarily on Ubuntu KDE (TuxedoOS) and Manjaro Openbox (Mabox) environments. 
While they may work on other Linux distributions, compatibility cannot be guaranteed. 
Users on different distributions may need to adjust paths or dependencies to suit their systems.

### Installation and Uninstallation:

> To install the VIS Configurator scripts on your system, run the `Install.sh` script. This script copies all necessary files to the appropriate directories and sets permissions as needed. After installation, you can use the VIS Configurator by running `vis-manager` from the menu.
>
> To uninstall the VIS Configurator scripts from your system, run the `Uninstall.sh` script. 
>
> This script removes all associated scripts from `/usr/local/bin` and `~/.local/share/applications/.desktop`.
>
> `Config files` need to be removed manualy.

### Vis-manager yad buttons explained.

1. **SAVE SETTINGS**: This button saves the configuration settings that you've chosen in the dialog. It updates various parameters such as the number of colors, spectrum bar width, bar spacing, smoothing mode, falloff mode, falloff weight, stereo mode, frames per second (FPS), and whether the visualizer spectrum is reversed. Once you've made your adjustments and click this button, your settings will be saved to the configuration file specified in the script.

2. **THEME SELECTOR**: Clicking this button executes a script for choosing a fixed theme. This likely allows you to select a predefined color scheme or visual theme for your visualization setup. Once you've selected a theme, it likely updates the configuration file with the chosen theme settings.

3. **RANDOM COLORS**: This button generates a set of random colors for your visualization. It shuffles a list of predefined colors and selects a specified number of them. These colors are then saved to a file (`mb-vis-colors`) which is used for coloring the visualization elements.

4. **SAVE THEME**: This button likely renames the colors in the visualization setup. It executes a script (`rename-vis-colors.sh`) that allows you to rename the colors used in your visualization scheme. Once you've renamed the colors, it updates the configuration file with the new color names.

5. **THEME CREATOR**: Clicking this button executes a script (`vis-yad-colors.sh`) for creating custom themes. It likely opens a dialog where you can specify custom colors for your visualization elements. Once you've created your custom theme, it updates the configuration file with the new color settings.

These buttons provide various options for customizing and managing the visual aspects of your setup, such as colors, themes, and other visual parameters.

### Scripts Overview:

1. **yad-select-file.sh**:
   - **Purpose:** Allows users to select a custom color theme for the VIS.
   - **Key Points:** Checks the validity of selected files as color themes before applying them.

2. **vis-yad-colors.sh**:
   - **Purpose:** Provides an interface for users to select and save colors for VIS customization.
   - **Key Points:** Validates color selection and displays error messages if necessary.

3. **vis-manager.sh**:
   - **Purpose:** Enables users to configure various settings for the VIS, such as colors, visualization modes, and frame rates.
   - **Key Points:** Validates input values and adjusts configuration files accordingly.

4. **vis-color-generator.sh**:
   - **Purpose:** Generates random colors and applies them to the VIS.
   - **Key Points:** Validates input values and adjusts configuration files accordingly.

5. **transparent-vis.sh**:
   - **Purpose:** Launches the VIS in a transparent window.
   - **Key Points:** Adjusts window settings for transparency and visual display.

6. **rename-vis-colors.sh**:
   - **Purpose:** Provides an interface for copying and renaming saved color themes for the VIS.
   - **Key Points:** Validates input file names and displays error messages if necessary.

7. **mb-setvar**:
   - **Purpose:** Sets configuration variables in the configuration file.
   - **Key Points:** Validates input variables and adjusts them in the configuration file.


## Contributions:

_Contributions to improve and expand the functionality of these scripts are welcome. 
If you encounter any issues or have ideas for enhancements, feel free to submit a pull request or open an issue on GitLab._





