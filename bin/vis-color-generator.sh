#!/bin/bash
# vis-color-generator.sh

CONFIG_DIR=~/.config/vis
CONFIG_FILE="$CONFIG_DIR/config"
COLORS_FILE="$CONFIG_DIR/colors/mb-vis-colors"

# Check if config exists, if not, create one
if [ ! -e "$CONFIG_FILE" ]; then
    touch "$CONFIG_FILE"
fi

# Backup existing mb-vis-colors
cp "$COLORS_FILE" "$COLORS_FILE.backup" 2>/dev/null

# Function to generate colors and save them to mb-vis-colors
generate_colors() {
    # Define a list of predefined colors
    colors=("FF0000" "00FF00" "0000FF" "FFFF00" "00FFFF" "FF00FF" "FFA500" "008000" "800080" "800000" "008080" "808000" "000080" "C0C0C0" "FFFFFF" 
            "FF6347" "00FA9A" "1E90FF" "FFD700" "4B0082" "FF4500" "8A2BE2" "20B2AA" "8B008B" "228B22" "2F4F4F" "B0E0E6" "8B4513" "808080" "4682B4" 
            "BC8F8F" "32CD32" "800000" "00CED1" "9932CC" "FF69B4" "9400D3" "BDB76B" "696969" "008080" "8B0000" "5F9EA0" "A52A2A" "2E8B57" "DAA520")

    # Shuffle the list using shuf command and select the first N colors
    shuf_colors=($(shuf -e "${colors[@]}" | head -n "$total_colors"))

    # Truncate the mb-vis-colors file before writing
    > "$COLORS_FILE"

    # Output the selected colors to the mb-vis-colors file
    for color in "${shuf_colors[@]}"; do
        echo "#$color" >> "$COLORS_FILE"
    done
}

# Check if mb-vis-colors exists, if not, create one
if [ ! -e "$COLORS_FILE" ]; then
    generate_colors > "$COLORS_FILE"
fi

# Read existing configuration values from config file
existing_colors_scheme=$(awk -F'=' '/colors\.scheme=/ {print $2}' "$CONFIG_FILE" | tr -d '[:space:]')
existing_bar_width=$(awk -F'=' '/visualizer\.spectrum\.bar\.width=/ {print $2}' "$CONFIG_FILE" | tr -d '[:space:]')
existing_bar_spacing=$(awk -F'=' '/visualizer\.spectrum\.bar\.spacing=/ {print $2}' "$CONFIG_FILE" | tr -d '[:space:]')
existing_smoothing_mode=$(awk -F'=' '/visualizer\.spectrum\.smoothing\.mode=/ {print $2}' "$CONFIG_FILE" | tr -d '[:space:]')
existing_falloff_mode=$(awk -F'=' '/visualizer\.spectrum\.falloff\.mode=/ {print $2}' "$CONFIG_FILE" | tr -d '[:space:]')
existing_falloff_weight=$(awk -F'=' '/visualizer\.spectrum\.falloff\.weight=/ {print $2}' "$CONFIG_FILE" | tr -d '[:space:]')
existing_stereo_mode=$(awk -F'=' '/audio\.stereo\.enabled=/ {print $2}' "$CONFIG_FILE" | tr -d '[:space:]')
existing_fps=$(awk -F'=' '/visualizer\.fps=/ {print $2}' "$CONFIG_FILE" | tr -d '[:space:]')
existing_amount_colors=$(awk -F'=' '/amount-colors=/ {print $2}' "$CONFIG_FILE" | tr -d '[:space:]')

# Function to show yad dialog and get the number of colors, visualizer, and additional settings
get_settings() {
    result=$(yad --form \
        --title="VIS basic Settings" \
        --text="<b>Setup VIS basic configuration.</b> " \
        --field="Amount of Colors (0-25):NUM" "$existing_amount_colors!1..45!1" \
        --field="Spectrum Bar Width (1-30):NUM" "$existing_bar_width!1..50!1" \
        --field="Spectrum Bar Spacing (0-20):NUM" "$existing_bar_spacing!0..10!1" \
        --field="Smoothing Mode:CB" "$existing_smoothing_mode!monstercat!sgs!none" \
        --field="Falloff Mode:CB" "$existing_falloff_mode!fill!top!none" \
        --field="Falloff Weight:CB" "$existing_falloff_weight!0.90!0.91!0.92!0.93!0.94!0.95!0.96!0.97!0.98!0.99" \
        --field="Stereo Mode:CB" "$existing_stereo_mode!mono!stereo" \
        --field="FPS:CB" "$existing_fps!12!13!14!15!16!17!18!19!20!21!22!23!24!25!26!27!28" \
        --button="gtk-ok:0" --button="gtk-cancel:1" \
        --center --width="300")
        
    # Check if the user clicked OK or Cancel
    if [ $? -eq 0 ]; then
        # Extract the selected values from the result
        total_colors=$(echo "$result" | awk -F'|' '{print $1}')
        bar_width=$(echo "$result" | awk -F'|' '{print $2}')
        bar_spacing=$(echo "$result" | awk -F'|' '{print $3}')
        smoothing_mode=$(echo "$result" | awk -F'|' '{print $4}')
        falloff_mode=$(echo "$result" | awk -F'|' '{print $5}')
        falloff_weight=$(echo "$result" | awk -F'|' '{print $6}')
        stereo_mode=$(echo "$result" | awk -F'|' '{print $7}')
        fps=$(echo "$result" | awk -F'|' '{print $8}')

        # Generate the selected colors
        generate_colors

        # Update config to use mb-vis-colors, chosen visualizer, and include additional settings
        sed -i -e '/colors\.scheme=/d' -e '/visualizer\.spectrum\.bar\.width=/d' -e '/visualizer\.spectrum\.bar\.spacing=/d' -e '/visualizers=/d' -e '/visualizer\.spectrum\.smoothing\.mode=/d' -e '/visualizer\.spectrum\.falloff\.mode=/d' -e '/visualizer\.spectrum\.falloff\.weight=/d' -e '/audio\.stereo\.enabled=/d' -e '/visualizer\.fps=/d' -e '/amount-colors=/d' "$CONFIG_FILE"
        echo "colors.scheme=mb-vis-colors" >> "$CONFIG_FILE"
        echo "visualizer.spectrum.bar.width=$bar_width" >> "$CONFIG_FILE"
        echo "visualizer.spectrum.bar.spacing=$bar_spacing" >> "$CONFIG_FILE"
        echo "visualizer.spectrum.smoothing.mode=$smoothing_mode" >> "$CONFIG_FILE"
        echo "visualizer.spectrum.falloff.mode=$falloff_mode" >> "$CONFIG_FILE"
        echo "visualizer.spectrum.falloff.weight=$falloff_weight" >> "$CONFIG_FILE"
        echo "audio.stereo.enabled=$stereo_mode" >> "$CONFIG_FILE"
        echo "visualizer.fps=$fps" >> "$CONFIG_FILE"
        echo "amount-colors=$total_colors" >> "$CONFIG_FILE"

        echo "mb-vis-colors generated and configured with $total_colors colors, bar width $bar_width, bar spacing $bar_spacing, smoothing mode $smoothing_mode, falloff mode $falloff_mode, falloff weight $falloff_weight, stereo mode $stereo_mode, and FPS $fps."
        notify-send -i mbcc -t 5000 "New cava colors and settings generated"

        # Run transparent-vis.sh in the background
        transparent-vis.sh &
    else
        echo "Operation canceled."
    fi
}

# Call the function to get the number of colors, visualizer, and additional settings
get_settings
