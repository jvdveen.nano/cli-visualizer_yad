#!/bin/bash
# vis-manager.sh

CONFIG_DIR="$HOME/.config/vis"
CONFIG_FILE="$CONFIG_DIR/mb-vis-config"
CNFG_AMOUNT="$CONFIG_DIR/.amount"
COLORS_FILE="$CONFIG_DIR/colors/mb-vis-colors"

# Ensure the CONFIG_DIR exists
mkdir -p "$CONFIG_DIR"

VISUALIZER_SPECTRUM_REVERSED=false

# Function to handle errors
handle_error() {
    local error_message="$1"
    echo "Error: $error_message" >&2
    exit 1
}

# Function to read and parse config variables from file
read_config() {
    visualizer_spectrum_reversed=$(awk -F'=' '/visualizer\.spectrum\.reversed=/ {print $2}' "$CONFIG_FILE" | tr -d '[:space:]')
    colors_scheme=$(awk -F'=' '/colors\.scheme=/ {print $2}' "$CONFIG_FILE" | tr -d '[:space:]')
    bar_width=$(awk -F'=' '/visualizer\.spectrum\.bar\.width=/ {print $2}' "$CONFIG_FILE" | tr -d '[:space:]')
    bar_spacing=$(awk -F'=' '/visualizer\.spectrum\.bar\.spacing=/ {print $2}' "$CONFIG_FILE" | tr -d '[:space:]')
    smoothing_mode=$(awk -F'=' '/visualizer\.spectrum\.smoothing\.mode=/ {print $2}' "$CONFIG_FILE" | tr -d '[:space:]')
    falloff_mode=$(awk -F'=' '/visualizer\.spectrum\.falloff\.mode=/ {print $2}' "$CONFIG_FILE" | tr -d '[:space:]')
    falloff_weight=$(awk -F'=' '/visualizer\.spectrum\.falloff\.weight=/ {print $2}' "$CONFIG_FILE" | tr -d '[:space:]')
    stereo_mode=$(awk -F'=' '/audio\.stereo\.enabled=/ {print $2}' "$CONFIG_FILE" | tr -d '[:space:]')
    fps=$(awk -F'=' '/visualizer\.fps=/ {print $2}' "$CONFIG_FILE" | tr -d '[:space:]')
    amount_colors=$(awk -F'=' '/amount-colors=/ {print $2}' "$CNFG_AMOUNT" | tr -d '[:space:]')
    visualizers=$(awk -F'=' '/visualizers=/ {print $2}' "$CONFIG_FILE" | tr -d '[:space:]')
    selected_char_spectrum=$(awk -F'=' '/visualizer\.spectrum\.character=/ {print $2}' "$CONFIG_FILE" | tr -d '[:space:]')
    selected_char_lorenz=$(awk -F'=' '/visualizer\.lorenz\.character=/ {print $2}' "$CONFIG_FILE" | tr -d '[:space:]')
    selected_char_ellipse=$(awk -F'=' '/visualizer\.ellipse\.character=/ {print $2}' "$CONFIG_FILE" | tr -d '[:space:]')
}


# Check if mb-vis-colors exists, if not, create one
if [ ! -e "$COLORS_FILE" ]; then
    generate_colors || handle_error "Failed to generate colors"
fi

# If amount file not exist, create one with defaults
if [ ! -f "$CNFG_AMOUNT" ]; then
    cat <<EOF > "${CNFG_AMOUNT}"
amount-colors=12
EOF
fi

# Read and parse config variables from file
read_config

# Function to generate colors and save them to mb-vis-colors
generate_colors() {
    # Define a list of predefined colors
    colors=("FF0000" "00FF00" "0000FF" "FFFF00" "00FFFF" "FF00FF" "FFA500" "008000" "800080" "800000" "008080" "808000" "000080" "C0C0C0" "FFFFFF" 
            "FF6347" "00FA9A" "1E90FF" "FFD700" "4B0082" "FF4500" "8A2BE2" "20B2AA" "8B008B" "228B22" "2F4F4F" "B0E0E6" "8B4513" "808080" "4682B4" 
            "BC8F8F" "32CD32" "800000" "00CED1" "9932CC" "FF69B4" "9400D3" "BDB76B" "696969" "008080" "8B0000" "5F9EA0" "A52A2A" "2E8B57" "DAA520")

    # Shuffle the list using shuf command and select the first N colors
    shuf_colors=($(shuf -e "${colors[@]}" | head -n "$amount_colors")) || handle_error "Failed to shuffle colors"

    # Truncate the mb-vis-colors file before writing
    > "$COLORS_FILE" || handle_error "Failed to truncate mb-vis-colors file: $COLORS_FILE"

    # Output the selected colors to the mb-vis-colors file
    for color in "${shuf_colors[@]}"; do
        echo "#$color" >> "$COLORS_FILE" || handle_error "Failed to write color to mb-vis-colors file: $COLORS_FILE"
    done
}

# Function to show yad dialog and get the number of colors, visualizer, and additional settings
get_settings() {
    read_config

    result=$(yad --form \
        --image="amarok_playcount" \
        --title=" Visualizer tweaker (vis) " \
        --text="<span background='#e4d7b0' foreground='#2a2a2a'><b> Setup VIS basic configuration.</b>  								<i> All visualizers(R=reload)  Spectrum(S=stereo/mono) </i> 								</span>" \
        --field=" Amount of Colors (1-27):NUM" "$amount_colors!1..27!1" \
        --field=" Spectrum Bar Width (1-250):NUM" "$bar_width!1..250!1" \
        --field=" Spectrum Bar Spacing (0-50):NUM" "$bar_spacing!0..50!1" \
        --field=" Smoothing Mode:CB" "$smoothing_mode!monstercat!sgs!none" \
        --field=" Falloff Mode:CB" "$falloff_mode!fill!top!none" \
        --field=" Falloff Weight:CB" "$falloff_weight!0.90!0.91!0.92!0.93!0.94!0.95!0.96!0.97!0.98!0.99" \
        --field=" Stereo Mode:CB" "$stereo_mode!true!false" \
        --field=" FPS:CB" "$fps!12!13!14!15!16!17!18!19!20!21!22!23!24!25!26!27!28" \
        --field=" Reversed:CHK" "$visualizer_spectrum_reversed" \
        --field=" Visualizers:CB" "$visualizers!spectrum!lorenz!ellipse" \
        	--field=" Character spectrum:CB" "$selected_char_spectrum!█!#!*!▒!░!▟!▀!▞!▢!●!◒!⦿!!!!!╋!╬" \
        --field=" Character lorenz:CB" "$selected_char_lorenz!█!#!*!▒!░!▟!▀!▞!▢!●!◒!⦿!!!!!╋!╬" \
        --field=" Character ellipse:CB" "$selected_char_ellipse!█!#!*!▒!░!▟!▀!▞!▢!●!◒!⦿!!!!!╋!╬" \
        --button="SAVE SETTINGS:0"  --button="RANDOM COLORS:1"  --button="SAVE THIS COLORS:4" \
         --button="THEME CREATOR:5" --button="THEME SELECTOR:2" --button="EXIT:3" \
        --center --width="350" )

    # Check if the user clicked OK, Cancel, Save, or Theme
    button=$?
    if [ $button -eq 0 ]; then
        # Extract the selected values from the result
        total_colors=$(echo "$result" | awk -F'|' '{print $1}')
        bar_width=$(echo "$result" | awk -F'|' '{print $2}')
        bar_spacing=$(echo "$result" | awk -F'|' '{print $3}')
        smoothing_mode=$(echo "$result" | awk -F'|' '{print $4}')
        falloff_mode=$(echo "$result" | awk -F'|' '{print $5}')
        falloff_weight=$(echo "$result" | awk -F'|' '{print $6}')
        stereo_mode_raw=$(echo "$result" | awk -F'|' '{print $7}')
        fps=$(echo "$result" | awk -F'|' '{print $8}')
        visualizer_spectrum_reversed=$(echo "$result" | awk -F'|' '{print $9}')
        selected_visualizer=$(echo "$result" | awk -F'|' '{print $10}')
        selected_char_spectrum=$(echo "$result" | awk -F'|' '{print $11}')
        selected_char_lorenz=$(echo "$result" | awk -F'|' '{print $12}')
        selected_char_ellipse=$(echo "$result" | awk -F'|' '{print $13}')

        # Convert stereo_mode to true/false
        stereo_mode=false
        [ "$stereo_mode_raw" == "true" ] && stereo_mode=true

        # Update config to use mb-vis-colors, chosen visualizer, and include additional settings
        mb-setvar "colors.scheme=mb-vis-colors" "$CONFIG_FILE" || handle_error "Failed to set colors scheme in config file: $CONFIG_FILE"
        mb-setvar "visualizer.spectrum.bar.width=$bar_width" "$CONFIG_FILE" || handle_error "Failed to set bar width in config file: $CONFIG_FILE"
        mb-setvar "visualizer.spectrum.bar.spacing=$bar_spacing" "$CONFIG_FILE" || handle_error "Failed to set bar spacing in config file: $CONFIG_FILE"
        mb-setvar "visualizer.spectrum.smoothing.mode=$smoothing_mode" "$CONFIG_FILE" || handle_error "Failed to set smoothing mode in config file: $CONFIG_FILE"
        mb-setvar "visualizer.spectrum.falloff.mode=$falloff_mode" "$CONFIG_FILE" || handle_error "Failed to set falloff mode in config file: $CONFIG_FILE"
        mb-setvar "visualizer.spectrum.falloff.weight=$falloff_weight" "$CONFIG_FILE" || handle_error "Failed to set falloff weight in config file: $CONFIG_FILE"
        mb-setvar "audio.stereo.enabled=$stereo_mode" "$CONFIG_FILE" || handle_error "Failed to set stereo mode in config file: $CONFIG_FILE"
        mb-setvar "visualizer.fps=$fps" "$CONFIG_FILE" || handle_error "Failed to set FPS in config file: $CONFIG_FILE"
        mb-setvar "amount-colors=$total_colors" "$CNFG_AMOUNT" || handle_error "Failed to set amount of colors in config file: $CNFG_AMOUNT"
        mb-setvar "visualizers=$selected_visualizer" "$CONFIG_FILE" || handle_error "Failed to set visualizers in config file: $CONFIG_FILE"
        mb-setvar "visualizer.spectrum.top.margin=0.02" "$CONFIG_FILE" || handle_error "Failed to set top margin in config file: $CONFIG_FILE"
        mb-setvar "visualizer.sgs.smoothing.points=7" "$CONFIG_FILE" || handle_error "Failed to set smoothing points in config file: $CONFIG_FILE"
        mb-setvar "visualizer.sgs.smoothing.passes=5" "$CONFIG_FILE" || handle_error "Failed to set smoothing passes in config file: $CONFIG_FILE"
        mb-setvar "visualizer.spectrum.reversed=$visualizer_spectrum_reversed" "$CONFIG_FILE" || handle_error "Failed to set reversed mode in config file: $CONFIG_FILE"
        mb-setvar "visualizer.spectrum.character=$selected_char_spectrum" "$CONFIG_FILE" || handle_error "Failed to set spectrum character in config file: $CONFIG_FILE"
        mb-setvar "visualizer.lorenz.character=$selected_char_lorenz" "$CONFIG_FILE" || handle_error "Failed to set lorenz character in config file: $CONFIG_FILE"
        mb-setvar "visualizer.ellipse.character=$selected_char_ellipse" "$CONFIG_FILE" || handle_error "Failed to set ellipse character in config file: $CONFIG_FILE"

        echo "Settings saved to $CONFIG_FILE."
        notify-send -i mbcc -t 5000 "VIS settings saved"

        get_settings

    elif [ $button -eq 1 ]; then
        generate_colors 
        transparent-vis.sh 200 & 
        get_settings

    elif [ $button -eq 2 ]; then
        # Execute the script for choosing a fixed theme
        yad-select-file.sh 
        get_settings

    elif [ $button -eq 4 ]; then
        rename-vis-colors.sh    
        get_settings

    elif [ $button -eq 5 ]; then
        vis-yad-colors.sh 
        get_settings

    else
        echo "Operation canceled."
    fi
}



# Call the function to get the number of colors, visualizer, and additional settings
get_settings
