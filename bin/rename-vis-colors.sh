#!/bin/bash
# rename-vis-colors.sh

CONFIG_DIR=~/.config/vis
COLORS_FILE="$CONFIG_DIR/colors/mb-vis-colors"

new_name=$(yad --title="Copy and Rename File" \
    --entry \
    --text="New File Name:" \
    --button="gtk-ok:0" --button="gtk-cancel:1" \
)

exit_status=$?

if [ $exit_status -eq 0 ]; then
    # Trim whitespace and special characters
    new_name=$(echo "$new_name" | tr -cd '[:alnum:]-_')
    new_file_path="$CONFIG_DIR/colors/$new_name"
    cp "$COLORS_FILE" "$new_file_path"
    if [ $? -eq 0 ]; then
        yad --title="Success" --text="File copied successfully to $new_file_path" --button="gtk-ok:0"
    else
        yad --title="Error" --text="Failed to copy file" --button="gtk-ok:0"
    fi
fi
