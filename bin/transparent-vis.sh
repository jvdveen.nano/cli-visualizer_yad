#!/bin/bash
# transparent-vis.sh

#: Usage: transparent-vis.sh (height in pixels) (width full or half).
#: example transparent-vis.sh 100 half.
#: original script meant for cava. creator @napcok (gitlab)
#: default is 200px height and full width.

CONFIG_DIR="$HOME/.config/terminator"
CONFIG_FILE="$CONFIG_DIR/vis-transparent"

if [ ! -f $CONFIG_FILE ]; then
cat <<EOF > ${CONFIG_FILE}
[global_config]
  dbus = False
[profiles]
  [[default]]
    allow_bold = False
    background_darkness = 0.0
    background_type = transparent
    cursor_blink = False
    cursor_color = "#aaaaaa"
    font = Sans 4
    show_titlebar = False
    scrollbar_position = hidden
    scroll_on_keystroke = False
    use_custom_command = True
    use_system_font = False
[layouts]
[plugins]
EOF
fi

CAVA_HEIGHT=${1:-200}
CAVA_WIDTH=${2:-full}
WIDTH=$(wmctrl -d|grep "*"|awk '{print $4}'|cut -d'x' -f1)
HEIGHT=$(wmctrl -d|grep "*"|awk '{print $4}'|cut -d'x' -f2)
TOP=$((HEIGHT-CAVA_HEIGHT))
LEFT=0
if [ "$CAVA_WIDTH" != "full" ];then
LEFT=$((WIDTH/4))
WIDTH=$((WIDTH/2))
else
:
fi 

terminator -b -g "$CONFIG_FILE" -T vistransparent --geometry "${WIDTH}x${CAVA_HEIGHT}+${LEFT}+${TOP}" -e "vis -c $HOME/.config/vis/mb-vis-config"
