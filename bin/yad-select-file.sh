#!/bin/bash
# yad-select-file.sh

CONFIG_DIR=~/.config/vis
THEME_DIR="$CONFIG_DIR/colors"
COLORS_FILE="$CONFIG_DIR/colors/mb-vis-colors"

# Get list of all files (excluding directories) in the THEME_DIR
files=("$THEME_DIR"/*)

# Create a string with file names
file_string="!$(IFS='!'; echo "${files[*]##*/}")!"

# Use Yad to create the window
selected_file=$(/usr/bin/yad --form \
                    --image="amarok_playcount" \
                    --text="<b>Custom color theme's.</b> " \
                    --field=" SELECT  :CB" "$file_string" \
                    --button="OK:0" --button="Cancel:1" \
                    --width=300 --height=100)

# Capture the return code of the yad command
button=$?
# Check if the user pressed OK
if [ $button -eq 0 ]; then
    # Extract the selected file from the dropdown field
    selected_file=$(echo "$selected_file" | cut -d"|" -f1)
    
    # Remove the leading and trailing '!' from the selected file
    selected_file=${selected_file#!}
    selected_file=${selected_file%!}
    
    # Check if the selected file is not a THEME_DIR
    if [ -f "$THEME_DIR/$selected_file" ]; then
        # Cat txt to config
       cat "$THEME_DIR/$selected_file" > "$COLORS_FILE"
        # Run transparent-vis.sh in the background
        transparent-vis.sh 200 &
    else
        echo "Error: Please select a valid file."
    fi
else
    # Print the message when "Cancel" is pressed
    echo "Operation canceled."
fi
