#!/bin/bash
# vis-yad-colors.sh

welcome="$HOME/.config/vis/welcome-vis.txt"

yad --title="Color Selector" \
    --text-info --filename="$welcome" \
    --button=" DO IT :0 " \
	--geometry="505x200"
	
CONFIG_DIR=~/.config/vis/colors
FILE_NAME="custom_colors"

while true; do
    color=$(yad --title="Add Color" \
        --color \
        --text="Select a color:" \
        --button="gtk-ok:0" --button="gtk-save:2" --button="gtk-cancel:1")
	
    exit_status=$?

    if [ $exit_status -eq 0 ]; then
        echo "$color" >> "/tmp/$FILE_NAME"
    elif [ $exit_status -eq 2 ]; then
        if [ $(wc -l < "/tmp/$FILE_NAME") -gt 0 ]; then
            new_name=$(yad --title="Save Colors" \
                --entry \
                --text="Enter file name:" \
                --button="gtk-ok:0" --button="gtk-cancel:1" \
				--center --width="350")

            if [ ! -z "$new_name" ]; then
                cp "/tmp/$FILE_NAME" "$CONFIG_DIR/$new_name"
                rm "/tmp/$FILE_NAME"
                yad --title="Success" --text="File saved as $new_name." --button="gtk-ok:0" --center --width="350"
            else
                rm "/tmp/$FILE_NAME"
                yad --title="Error" --text="File name cannot be empty." --button="gtk-ok:0" --center --width="350"
            fi
        else
            yad --title="Error" --text="No colors selected." --button="gtk-ok:0" --center --width="350"
        fi
    else
        break
    fi
done
